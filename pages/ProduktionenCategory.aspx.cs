using System;
using System.Globalization;
using System.Web.UI;
using ourCompany;
using System.Xml.XPath;
using System.Linq;

[PartialCaching(604800, "*", "", "infoAge")]
public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CultureInfo deCulture = new CultureInfo("de-de");

        IDataNodeIterator
            productions = _data.Select("root/programm/production");


        new CSSControl()
            .Tag(
                "nav.col.sub-menu",
                _ =>
                {
                    _
                         .AddAttribute("href", "/" + _lang + "/produktionen")
                         .Tag("a.sub-menu-link", "&#252;bersicht");
                    foreach (IDataNode productionNode in productions)
                    {
                        _
                            .AddAttribute("href", "/" + _lang + "/produktionen/" + productionNode.GetAttribute("UID"))
                            .Tag("a.sub-menu-link" + (productionNode.GetAttribute("UID") == _submenu ? ".active" : ""), productionNode.GetAttribute("title"));

                    }
                })
            .AppendToPlaceHolder(submenuPH);

        IDataNode production = _data.SelectSingleNode("root/programm/production[@UID = '" + _submenu + "']");
        if (production == null)
        {
            Response.Redirect("/" + _lang + "/produktionen");
            return;
        }
        _title += " / Produktionen / " + production.GetAttribute("title");
        XPathExpression
            datesXpress = XPathExpression.Compile("date");

        datesXpress.AddSort("translate(@date,'-','0')", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Number);
        

        IDataNodeIterator
            events = ((DataNode)production).Select("event");

        // sort events 
        IOrderedEnumerable<IDataNode> sortedEvents = events
                    .OrderBy(node => {
                        IDataNode date = ((DataNode)node).Select(datesXpress).FirstOrDefault();
                        if (date != null)
                            return date.GetAttribute("date").Replace("-", "0");
                        else
                            return "0";
                    });


        new CSSControl()
            .Tag("article.page", _ => _
                .Tag("header.page-title-group", () => _
                    .Tag("h1.page-title", production.GetAttribute("title"))
                )
                .Apply(c =>
                {
                    if (production.GetValue("text").Check() && production.GetValue("text") != " " && production.GetValue("text") != "&nbsp;")
                    {
                        c
                            .Tag("article.intro-text.row", () => _
                                .Tag("div.col.col-1-2", production.GetValue("text"))
                            );
                    }
                })
                .Apply(() =>
                           {
                               /* for old layout
                               CSSControl
                                   col_1 = new CSSControl(),
                                   col_2 = new CSSControl();
                               int
                                       counter = 0,  
                                       count = events.Count;
                                */


                               foreach (IDataNode eventNode in sortedEvents)
                               {
                                   IDataNode
                                       image = eventNode.SelectSingleNode("image");
                                   _.Tag("article.row.hover-color",
                                         () => _
                                               .Tag("section.col.col-1-2",
                                               () => _
                                                   .Tag("div.mehr-container", () => _
                                                       .Tag("header",
                                                        () => _
                                                           .Tag("h1.page-list-title", eventNode.GetAttribute("title").ToUpperInvariant())
                                                           .Tag("h2.untertitel", eventNode.GetAttribute("intro"))
                                                           .Tag("h2", eventNode.GetAttribute("subtitle")))
                                                       .Apply(() =>
                                                       {
                                                           foreach (IDataNode dateEvent in ((DataNode)eventNode).Select(datesXpress))
                                                           {
                                                               string dateTitle = dateEvent.GetAttribute("title").Check() ? dateEvent.GetAttribute("title") : dateEvent.GetAttribute("time") + " Uhr";
                                                               DateTime currentDate;
                                                               if (!DateTime.TryParse(dateEvent.GetAttribute("date"), out currentDate))
                                                                   continue;
                                                               _.Tag("time.event-date", () =>
                                                                    _
                                                                    .Tag("span.day", currentDate.ToString("ddd", deCulture))
                                                                    .Tag("span.day-num", currentDate.ToString("dd.", deCulture))
                                                                    .Tag("span.month", currentDate.ToString("MMMM", deCulture))
                                                                    .Tag("span.hour", dateTitle)
                                                                );
                                                           }
                                                       })
                                                       .Tag("span.event-place", eventNode.GetAttribute("place"))
                                                       .AddAttribute("href", "/" + _lang + "/produktion/" + eventNode.GetAttribute("UID"))
                                                       .Tag("a.ir.ir-cover", "mehr...")
                                                    )
                                                    .Apply(() =>
                                                    {
                                                        if (eventNode.GetAttribute("ticket").Check())
                                                        {
                                                            _.AddAttribute("target", "_blank")
                                                             .AddAttribute("href", eventNode.GetAttribute("ticket"))
                                                             .Tag("a.ticket-link", "Tickets kaufen");
                                                        }
                                                    })
                                               )
                                               .Apply(image != null,
                                                   () => _.Tag("figure.col.col-1-2",
                                                               () => _
                                                                         .AddAttribute("href", "/" + _lang + "/produktion/" + eventNode.GetAttribute("UID"))
                                                                         .Tag("a", () => _
                                                                             .AddAttribute("src", image.GetAttribute("src") + "?width=140")
                                                                             .Tag("img")))
                                                                         )
                                       //.Tag("section.col.col-1-2",
                                       //() => _
                                       //    .Tag("time.page-list-title", dateEvent.GetAttribute("time") + " Uhr")
                                       //    .Tag("span.event-place", )
                                       //    .Tag("header",
                                       //         () => _
                                       //                   .Tag("h1.inner-title", eventNode.GetAttribute("title"))
                                       //                   .Tag("h2", eventNode.GetAttribute("subtitle")))
                                       //    .AddAttribute("href", "/produktion/" + eventNode.GetAttribute("UID"))
                                       //    .Tag("a.ir.ir-cover", "mehr...")
                                       //)
                                       );
                               }
                               #region old layout
                               //old layout
                               //foreach (IDataNode eventNode in events)
                               //{
                               //    CSSControl col = counter%2 == 0 ? col_1 : col_2;
                               //    IDataNode
                               //        image = eventNode.SelectSingleNode("image");

                               //    col.Tag("article.production-event.hover-color",
                               //          () => col
                               //                .Tag("section",
                               //                () => col
                               //                    .Tag("header",
                               //                         () => col
                               //                            .Tag("h1.event-place", eventNode.GetAttribute("title").ToUpperInvariant())
                               //                            .Tag("h2", eventNode.GetAttribute("subtitle")))
                               //                    .Apply(() =>
                               //                               {
                               //                                   foreach (IDataNode dateEvent in eventNode.Select("date"))
                               //                                   {
                               //                                       DateTime currentDate;
                               //                                       if (!DateTime.TryParse(dateEvent.GetAttribute("date"), out currentDate))
                               //                                           continue;
                               //                                       col.Tag("time.event-date",
                               //                                             currentDate.ToString("dddd dd. MMMM ", deCulture).ToUpperInvariant() +
                               //                                             " " + dateEvent.GetAttribute("time") + " Uhr");
                               //                                   }
                               //                               })
                               //                    .Tag("span.event-place", eventNode.GetAttribute("place"))
                               //                    .AddAttribute("href", "/produktion/" + eventNode.GetAttribute("UID"))
                               //                    .Tag("a.ir.ir-cover", "mehr...")
                               //                )
                               //                .Apply(image != null,
                               //                    () => col.Tag("figure",
                               //                                () => col
                               //                                          .AddAttribute("src", image.GetAttribute("src") + "?width=140")
                               //                                          .Tag("img")))

                               //        );
                               //    counter++;
                               //}
                               //_
                               //    .Parse("div.row")
                               //    .Parse("div.col.col-1-2")
                               //    .AppendCSSControl(col_1)
                               //    .Parse("/div.col.col-1-2")
                               //    .Parse("div.col.col-1-2")
                               //    .AppendCSSControl(col_2)
                               //    .Parse("/div.col.col-1-2")
                               //    .Parse("/div.row");
                               #endregion

                           })
            )
            .AppendToPlaceHolder(pagePH);
    }
}