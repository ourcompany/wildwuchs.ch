using System;
using System.Globalization;
using System.Web.UI;
using ourCompany;
using System.Xml.XPath;
using System.Linq;

[PartialCaching(604800, "*", "", "infoAge")]
public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CultureInfo deCulture = new CultureInfo("de-de");

        IDataNodeIterator
            productions = _data.Select("root/programm/production");

        _title += " / Produktionen";
        new CSSControl()
            .Tag(
                "nav.col.sub-menu",
                _ =>
                {
                    _.Tag("span.sub-menu-link.active", "&#252;bersicht");
                    foreach (IDataNode production in productions)
                    {
                        _
                            .AddAttribute("href", "/" + _lang + "/produktionen/" + production.GetAttribute("UID"))
                            .Tag("a.sub-menu-link", production.GetAttribute("title"));

                    }
                })
            .AppendToPlaceHolder(submenuPH);

        XPathExpression
            datesXpress = XPathExpression.Compile("date");

        datesXpress
            .AddSort("number(translate(@date,'-','0'))*10000 + number(translate(@time,':','0'))", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Number);

        


        IDataNodeIterator
            events = _data.Select("root/programm/production/event");

        // sort events 
        IOrderedEnumerable<IDataNode> sortedEvents = events
                    .OrderBy(node =>
                    {
                        IDataNode date = ((DataNode)node).Select(datesXpress).FirstOrDefault();
                        if (date != null)
                            return date.GetAttribute("date").Replace("-", "0");
                        else
                            return "9999999999";
                    });

        new CSSControl()
            .Tag("article.page", _ => _
                .Tag("header.page-title-group", () => _
                    .Tag("h1.page-title", "&#252;bersicht")
                )
                .Apply(() =>
                    {
                        foreach (IDataNode eventNode in sortedEvents)
                        {
                            IDataNode
                                image = eventNode.SelectSingleNode("image");
                            _.Tag("article.row.hover-color",
                                  () => _
                                        .Tag("section.col.col-1-2",
                                        () => _
                                            .Tag("div.mehr-container", () => _
                                                .Tag("header",
                                                 () => _
                                                    .Tag("h1.page-list-title", eventNode.GetAttribute("title").ToUpperInvariant())
                                                    .Tag("h2.untertitel", eventNode.GetAttribute("intro"))
                                                    .Tag("h2", eventNode.GetAttribute("subtitle")))
                                                .Apply(() =>
                                                {
                                                    foreach (IDataNode dateEvent in ((DataNode)eventNode).Select(datesXpress))
                                                    {
                                                        string dateTitle = dateEvent.GetAttribute("title").Check() ? dateEvent.GetAttribute("title") : dateEvent.GetAttribute("time") + " Uhr";
                                                        DateTime currentDate;
                                                        if (!DateTime.TryParse(dateEvent.GetAttribute("date"), out currentDate))
                                                            continue;
                                                        _.Tag("time.event-date", () =>
                                                            _
                                                                .Tag("span.day", currentDate.ToString("ddd", deCulture))
                                                                .Tag("span.day-num", currentDate.ToString("dd.", deCulture))
                                                                .Tag("span.month", currentDate.ToString("MMMM", deCulture))
                                                                .Tag("span.hour", dateTitle)
                                                            );
                                                    }
                                                })
                                                .Tag("span.event-place", eventNode.GetAttribute("place"))
                                                .AddAttribute("href", "/" + _lang + "/produktion/" + eventNode.GetAttribute("UID"))
                                                .Tag("a.ir.ir-cover", "mehr...")
                                            )
                                            .Apply(() =>
                                            {
                                                if (eventNode.GetAttribute("ticket").Check())
                                                {
                                                    _.AddAttribute("target", "_blank")
                                                     .AddAttribute("href", eventNode.GetAttribute("ticket"))
                                                     .Tag("a.ticket-link", "Tickets kaufen");
                                                }
                                            })
                                        )
                                        .Apply(image != null,
                                            () => _.Tag("figure.col.col-1-2",
                                                        () => _
                                                                  .AddAttribute("href", "/" + _lang + "/produktion/" + eventNode.GetAttribute("UID"))
                                                                  .Tag("a", () => _
                                                                      .AddAttribute("src", image.GetAttribute("src") + "?width=140")
                                                                      .Tag("img")))
                                                                  )
                                //.Tag("section.col.col-1-2",
                                //() => _
                                //    .Tag("time.page-list-title", dateEvent.GetAttribute("time") + " Uhr")
                                //    .Tag("span.event-place", )
                                //    .Tag("header",
                                //         () => _
                                //                   .Tag("h1.inner-title", eventNode.GetAttribute("title"))
                                //                   .Tag("h2", eventNode.GetAttribute("subtitle")))
                                //    .AddAttribute("href", "/produktion/" + eventNode.GetAttribute("UID"))
                                //    .Tag("a.ir.ir-cover", "mehr...")
                                //)
                                );
                        }
                    })
            )
            .AppendToPlaceHolder(pagePH);
    }
}