using System;
using System.Globalization;
using System.Web.UI;
using ourCompany;
using System.Xml.XPath;

[PartialCaching(604800, "*", "", "infoAge")]
public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CultureInfo deCulture = new CultureInfo("de-de");
        XPathExpression
            datesXpress = XPathExpression.Compile("root/programm/production/event/date[not(@date = preceding::date/@date)]");

        datesXpress.AddSort("translate(@date,'-','0')", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Number);

        IDataNodeIterator
            dates = _data.Select(datesXpress);

        _title += " / Programm";

        new CSSControl()
            .Tag(
                "nav.col.sub-menu",
                _ =>
                {
                    _
                        .AddAttribute("href", "/kalender")
                        .Tag("a.sub-menu-link", "&#252;bersicht");
                    foreach (IDataNode date in dates)
                    {
                        DateTime parsedDate;
                        string datString = date.GetAttribute("date");
                        if (DateTime.TryParse(datString, out parsedDate))
                            _
                                .AddAttribute("href", "/" + _lang + "/kalender/" + datString)
                                .Tag("a.sub-menu-link" + (datString == _submenu ? ".active" : ""), () => _
                                    .Tag("span.day", parsedDate.ToString("ddd", deCulture))
                                    .Tag("span.day-num", parsedDate.ToString("dd.", deCulture))
                                    .Tag("span.month", parsedDate.ToString("MMMM", deCulture))
                                );
                    }
                })
            .AppendToPlaceHolder(submenuPH);

        DateTime currentDate;
        if (!DateTime.TryParse(_submenu, out currentDate))
        {
            Response.Redirect("/kalender");
            return;
        }
        _title += " / Kalendar / " + currentDate.ToString("ddd dd. MMMM", deCulture);
        XPathExpression
            dateEventsXpress = XPathExpression.Compile("root/programm/production/event/date[@date = '" + _submenu + "']");

        dateEventsXpress.AddSort("translate(@time,':','0')", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Number);

        IDataNodeIterator dateEvents = _data.Select(dateEventsXpress);


        new CSSControl()
            .Tag("article.page", _ => _
                .Tag("header.page-title-group", () => _
                    .Tag("h1.page-title", currentDate.ToString("dddd dd. MMMM ", deCulture))
                )
                .Apply(() =>
                           {
                               int counter = 0,
                                   count = dateEvents.Count;
                               _.Parse("div.row");
                               foreach (IDataNode dateEvent in dateEvents)
                               {
                                   string dateTitle = dateEvent.GetAttribute("title").Check() ? dateEvent.GetAttribute("title") : dateEvent.GetAttribute("time") + " Uhr";
                                   IDataNode eventNode = dateEvent.SelectSingleNode("..");
                                   _.Tag("article.col.col-1-3.hover-color",
                                           () => _
                                           .Tag("time.page-list-title", dateTitle)
                                           .Tag("span.event-place", eventNode.GetAttribute("place"))
                                           .Tag("header",
                                           () => _
                                               .Tag("h1.inner-title", eventNode.GetAttribute("title"))
                                               .Tag("h2", eventNode.GetAttribute("subtitle")))
                                           .AddAttribute("href", "/" + _lang + "/produktion/" + eventNode.GetAttribute("UID"))
                                           .Tag("a.ir.ir-cover", "mehr...")
                                       );
                                   counter++;
                                   if (counter != 0 && counter % 3 == 0 && counter != count)
                                       _.Parse("/div.row").Parse("div.row");
                               }
                               _.Parse("/div.row");
                           })
            )
            .AppendToPlaceHolder(pagePH);
    }
}