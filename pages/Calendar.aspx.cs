using System;
using System.Globalization;
using System.Web.UI;
using ourCompany;
using System.Xml.XPath;

[PartialCaching(604800, "*", "", "infoAge")]
public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CultureInfo deCulture = new CultureInfo("de-de");
        XPathExpression
            datesXpress = XPathExpression.Compile("root/programm/production/event/date[not(@date = preceding::date/@date)]");

        datesXpress.AddSort("translate(@date,'-','0')", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Number);

        IDataNodeIterator
            dates = _data.Select(datesXpress);
        _title += " / Kalendar";
        new CSSControl()
            .Tag(
                "nav.col.sub-menu",
                _ =>
                {
                    _.Tag("span.sub-menu-link.active", "&#252;bersicht");
                    foreach (IDataNode date in dates)
                    {
                        DateTime parsedDate;
                        if (DateTime.TryParse(date.GetAttribute("date"), out parsedDate))
                            _
                                .AddAttribute("href","/" + _lang + "/kalender/" + date.GetAttribute("date"))
                                .Tag("a.sub-menu-link", () => _
                                    .Tag("span.day", parsedDate.ToString("ddd", deCulture))
                                    .Tag("span.day-num", parsedDate.ToString("dd.", deCulture))
                                    .Tag("span.month", parsedDate.ToString("MMMM", deCulture))
                                );
                    }
                })
            .AppendToPlaceHolder(submenuPH);


        new CSSControl()
            .Tag("article.page", _ => _
                .Tag("header.page-title-group", () => _
                    .Tag("h1.page-title", "&#252;bersicht")
                )
                .Apply(() =>
                    {
                        foreach(IDataNode date in dates)
                        {
                            _.Tag("section.calendar-date.row.clearfix",
                                () => _
                                    .Tag("header.col.col-1-1",
                                        () => {
                                                DateTime parsedDate;
                                                if (DateTime.TryParse(date.GetAttribute("date"), out parsedDate))
                                                    _.Tag("time.page-list-title", parsedDate.ToString("dddd dd. MMMM ", deCulture).ToUpperInvariant());
                                            })
                                    .Apply(() =>
                                               {
                                                   XPathExpression
                                                        dateEventsXpress = XPathExpression.Compile("root/programm/production/event/date[@date = '" + date.GetAttribute("date") + "']");

                                                   dateEventsXpress.AddSort("translate(@time,':','0')", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Number);

                                                   IDataNodeIterator dateEvents = _data.Select(dateEventsXpress);
                                                   int counter = 0,
                                                       count = dateEvents.Count;
                                                   _.Parse("div.clearfix");
                                                   foreach(IDataNode dateEvent in dateEvents)
                                                   {
                                                       string dateTitle = dateEvent.GetAttribute("title").Check() ? dateEvent.GetAttribute("title") : dateEvent.GetAttribute("time") + " Uhr";
                                                       IDataNode eventNode = dateEvent.SelectSingleNode("..");
                                                       _.Tag("article.col.col-1-3.hover-color",
                                                             () => _
                                                                .Tag("time.page-list-title", dateTitle)
                                                                .Tag("span.event-place", eventNode.GetAttribute("place"))
                                                                .Tag("header", 
                                                                () => _
                                                                    .Tag("h1.inner-title", eventNode.GetAttribute("title"))
                                                                    .Tag("h2", eventNode.GetAttribute("subtitle")))
                                                                .AddAttribute("href","/" + _lang + "/produktion/" + eventNode.GetAttribute("UID"))
                                                                .Tag("a.ir.ir-cover", "mehr...")
                                                           );
                                                       counter++;
                                                       if (counter != 0 && counter % 3 == 0 && counter != count)
                                                           _.Parse("/div.clearfix").Parse("div.clearfix");
                                                   }
                                                   _.Parse("/div.clearfix");
                                               }));
                        }
                    })
            )
            .AppendToPlaceHolder(pagePH);
    }
}