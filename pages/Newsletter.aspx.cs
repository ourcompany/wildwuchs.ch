﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ourCompany;

public partial class pages_Newsletter : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _title += " / Newsletter" ;
        new CSSControl()
            .Tag("article.page.newsletter-page", _ => _
                .Tag("header.page-title-group", () => _
                    .Tag("h1.col-1-1.page-title", "NEWSLETTER")
                )
                .Tag("div.row", () => _
                    .Tag("div#newsletter-form.col.col-1-2", () => _
                        .Tag("h2.newsletter-subtitle", () => _
                            .Write("Anmelden ")
                            .Tag("span#success.success", "erfolgreich")
                        )
                        .Tag("legend.clearfix", () => _
                            .AddAttribute("type", "text")
                            .AddAttribute("name", "email")
                            .Tag("input.to-check")
                            .Tag("label", "EMAIL:")
                        )
                        .Tag("legend.clearfix", () => _
                            .AddAttribute("type", "text")
                            .AddAttribute("name", "name")
                            .Tag("input.to-check")
                            .Tag("label", "NAME:")
                        )
                        .AddAttribute("href", "javascript:site.call('newsletter', { a : 'send' })")
                        .Tag("a#send.send", "SENDEN")
                        .Tag("div#loader.loader", "SENDEN")
                        .Tag("div#thanks.thanks", "DANKE!")
                    )
                )
            )
            .AppendToPlaceHolder(pagePH);

        Assets.AddJavascript(this.Page, "site.addEvent('domready', function() { new newsletter(); })");
    }
}