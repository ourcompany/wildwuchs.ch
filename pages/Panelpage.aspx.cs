﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ourCompany;
using System.Text;

public partial class pages_PanelPage : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool 
            isSubmenu = _submenu.Check();
        IDataNode 
            pageNode;
        if (isSubmenu)
            pageNode = _data.SelectSingleNode("root/pages/page[@UID='" + _menu + "']/page[@UID='" + _submenu + "']");
        else
            pageNode = _data.SelectSingleNode("root/pages/page[@UID='" + _menu + "']");


        if (pageNode == null)
        {
            Response.Redirect("/");
            return;
        }
        _title += " / " + pageNode.GetAttribute("title");
        if (!isSubmenu && pageNode.Select("content/*").Count == 0)
        {
            pageNode = pageNode.SelectSingleNode("page");
            isSubmenu = true;
            if (pageNode == null)
            {
                return;
            }
            _submenu = pageNode.GetAttribute("UID");
        }

        if (isSubmenu || pageNode.Select("page").Count > 0)
        {
            IDataNodeIterator 
                submenus;

            if (isSubmenu)
                submenus = pageNode.Select("../page");
            else
                submenus = pageNode.Select("page");

            new CSSControl()
                .Parse("nav.col.sub-menu")
                    .Apply(c => {
                        bool isActive = false;
                        foreach (IDataNode submenu in submenus)
                        {
                            string submenuUID = submenu.GetAttribute("UID");
                            isActive = submenuUID == _submenu;
                            c
                                .AddAttribute("href", "/" + _lang + "/p/" + _menu + "/" + submenuUID)
                                .Tag("a.sub-menu-link" + (isActive ? ".active" : ""), submenu.GetAttribute("title"));
                        }
                    })
                .Parse("/nav.col.sub-menu")
                .AppendToPlaceHolder(submenuPH);

        }

        

        new CSSControl()
            .Parse("article.page.detail-page")
                .Parse("header.page-title-group")
                    .Tag("h1.col-1-1.page-title", pageNode.GetAttribute("title"))
                    .Tag("h2.col-1-1.page-subtitle", pageNode.GetAttribute("subtitle"))
                .Parse("/header.page-title-group")
                .Parse("div.row")
                    .Parse("div.col.col-1-2.detail-page-text")
                        .Parse("div.row")
                            .Apply(c => {
                                IDataNodeIterator panels = isSubmenu ? pageNode.Select("textpanel|imagepanel|diaporama") : pageNode.Select("content/textpanel|content/imagepanel|content/diaporama");
                                RenderPanels(ref c, panels);
                            })
                        .Parse("/div.row")
                    .Parse("/div.col.col-1-2.detail-page-text")
                    .Apply(c => {
                        var
                            extracol = isSubmenu ? pageNode.SelectSingleNode("extracol") : pageNode.SelectSingleNode("content/extracol");
                        if (extracol != null)
                        {
                            c
                                .Parse("div.col.col-1-2.detail-page-text")
                                .Parse("div.row");
                            var 
                                panels = extracol.Select("textpanel|imagepanel|diaporama");
                            RenderPanels(ref c, panels);
                            c
                                .Parse("/div.row")
                                .Parse("/div.col.col-1-2.detail-page-text");
                        }
                    })
                .Parse("/div.row")
            .Parse("/article.page.detail-page")
            .AppendToPlaceHolder(pagePH);

        Assets.AddJavascript(this.Page, "site.addEvent('domready', function() { new lightbox(); })");
    }

    private static void RenderPanels(ref CSSControl CSSC, IDataNodeIterator panels) {
        int count = 0;
        foreach (IDataNode panel in panels)
        {
            switch (panel.Name)
            {
                case "textpanel":
                    CSSC
                        .Parse("div.col.col-1-2")
                            .Tag("div.detail-page-paragraph", panel.GetValue("text"))
                        .Parse("/div");
                    break;
                case "imagepanel":
                    CSSC
                        .Parse("figure.col.col-1-2")
                            .AddAttribute("src", panel.GetAttribute("src") + "?maxwidth=310")
                            .Tag("img")
                        .Parse("/figure");
                    break;
                case "diaporama":
                    CSSC
                        .Parse("article.diaporama-outer.col.col-1-2")
                            .Tag("h1.diaporama-title", panel.GetAttribute("title"));

                    var firstImage = panel.SelectSingleNode("image");
                    var images = panel.Select("image");
                    StringBuilder diaporamaContent = new StringBuilder();
                    diaporamaContent.Append("[");
                    int imagecount = 0;
                    foreach (var image in images)
                    {
                        string src = image.GetAttribute("src");
                        if(src.Check()) { 
                            diaporamaContent.Append("'");
                            diaporamaContent.Append(src);
                            diaporamaContent.Append("'");
                        }
                        imagecount++;
                        if (imagecount != images.Count)
                            diaporamaContent.Append(",");
                    }
                    diaporamaContent.Append("]");

                    CSSC
                        .AddAttribute("href", "javascript:site.call('lightbox', {'a' : 'diaporama', i : " + diaporamaContent.ToString() + "})")
                        .Parse("a.diaporama-link")
                            .AddAttribute("src", firstImage.GetAttribute("src") + "?maxwidth=310")
                            .Tag("img")
                        .Parse("/a.diaporama-link")
                        .Parse("/article.diaporama-outer");
                    break;
            }
            count++;
        }
    }
}