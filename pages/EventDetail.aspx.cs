﻿using System;
using System.Globalization;
using ourCompany;


public partial class pages_EventDetail : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CultureInfo deCulture = new CultureInfo("de-de");
        IDataNode eventNode = _data.SelectSingleNode("root/programm/production/event[@UID='" + _submenu + "']");

        if (eventNode == null)
        {
            Response.Redirect("/produktionen");
            return;
        }
        _title += " / " + eventNode.GetAttribute("title");
        IDataNode productionNode = eventNode.SelectSingleNode("..");
        string productionUID = productionNode.GetAttribute("UID");

        var uri = Request.Url;

        IDataNodeIterator productions = _data.Select("root/programm/production");

        IDataNode
            nextEvent = eventNode.SelectSingleNode("following::event[1]") ?? eventNode.SelectSingleNode("preceding::event[last()]") ?? eventNode,
            prevEvent = eventNode.SelectSingleNode("preceding::event[1]") ?? eventNode.SelectSingleNode("following::event[last()]") ?? eventNode;

        new CSSControl()
            .Tag("nav.col.sub-menu",
                _ =>
                {
                    _
                        .AddAttribute("href", "/" + _lang + "/produktionen")
                        .Tag("a.sub-menu-link", "&#252;bersicht");
                    foreach (IDataNode production in productions)
                    {
                        
                        bool isActive = productionUID == production.GetAttribute("UID");
                        _
                            .AddAttribute("href", "/" + _lang + "/produktionen/" + production.GetAttribute("UID"))
                            .Tag("a.sub-menu-link" + (isActive ? ".active" : ""), production.GetAttribute("title"));

                        
                    }
                })
            .AppendToPlaceHolder(submenuPH);

        new CSSControl()
            .Tag("article.page.detail-page", _ => _
                .Tag("header.page-title-group", () => _
                    .Tag("h1.col-1-1.page-title", eventNode.GetAttribute("title"))
                    .Tag("h2.col-1-1.page-subtitle.untertitel", eventNode.GetAttribute("intro"))
                    .Tag("h2.col-1-1.page-subtitle", eventNode.GetAttribute("subtitle"))
                )
                .Tag("div.row", () => _
                    .Tag("div.col.col-1-2.detail-page-text", () => _
                        .Tag("div.row", () => _
                            .Tag("div.col.col-1-2", () => _
                                .Tag("div.detail-page-paragraph", () =>
                                {
                                    IDataNodeIterator dates = eventNode.Select("date");
                                    foreach (IDataNode date in dates)
                                    {
                                        string time = date.GetAttribute("title").Check() ? date.GetAttribute("title") : date.GetAttribute("time") + " Uhr";
                                        DateTime parsedDate;
                                        if (DateTime.TryParse(date.GetAttribute("date"), out parsedDate))
                                             _.Tag("time.event-date", () =>
                                                _
                                                .Tag("span.day", parsedDate.ToString("ddd", deCulture))
                                                .Tag("span.day-num", parsedDate.ToString("dd.", deCulture))
                                                .Tag("span.month", parsedDate.ToString("MMMM", deCulture))
                                                .Tag("span.hour", time)
                                            );
                                    }
                                    _.Tag("div.event-place", eventNode.GetAttribute("place"));
                                    if (eventNode.GetAttribute("ticket").Check())
                                    {
                                        _.AddAttribute("target", "_blank")
                                         .AddAttribute("href", eventNode.GetAttribute("ticket"))
                                         .Tag("a.ticket-link", "Tickets kaufen");
                                    }
                                    _.Tag("div.event-text", eventNode.GetValue("text"));
                                    if (eventNode.GetValue("bio").Check() && eventNode.GetValue("bio").Length > 1)
                                    {
                                        _.Tag("div.event-bio", () =>
                                        {
                                            _
                                                .AddAttribute("href", "javascript:site.call('toggle', { l : 'event-bio-link', c : 'event-bio-content'})")
                                                .Tag("a.event-bio-link", "Bio")
                                                .Tag("div.event-bio-content", eventNode.GetValue("bio"));
                                        });
                                    }
                                    if (eventNode.GetValue("credit").Check() && eventNode.GetValue("credit").Length > 1)
                                    {
                                        _.Tag("div.event-credit", () =>
                                        {
                                            _
                                                .AddAttribute("href", "javascript:site.call('toggle', { l : 'event-credit-link', c : 'event-credit-content'})")
                                                .Tag("a.event-credit-link", "Credits")
                                                .Tag("div.event-credit-content", eventNode.GetValue("credit"));
                                        });
                                    }                                        
                                })
                            )
                        )
                    )
                    .Tag("div.col.col-1-2.detail-page-images", () => _
                        .Tag("div.row", () => _ 
                            .Apply(() =>
                            {
                                IDataNodeIterator images = eventNode.Select("image|video");
                                foreach (IDataNode image in images)
                                {
                                    string src = image.GetAttribute("src");
                                    switch (image.Name)
                                    {
                                        case "image":
                                            _.Tag("figure.col.col-1-2", () => _
                                                .AddAttribute("href", "javascript:site.call('lightbox', {a : 'open', i : '" + src + "'})")
                                                .Tag("a.lightbox-link", () => _
                                                    .AddAttribute("src", src + "?width=" + constant.IMAGE_COL_MAX_WIDTH)
                                                    .Tag("img")
                                                )
                                            );
                                            break;
                                        case "video":
                                            _.Tag("figure.video.col.col-1-2", () => _
                                                .AddAttribute("src", src + "?width=" + constant.IMAGE_COL_MAX_WIDTH)
                                                .Tag("img")
                                                .AddAttribute("href", "javascript:site.call('lightbox', {a : 'video', i : '" + image.GetAttribute("video") + "'})")
                                                .Tag("a.lightbox-link.video-link")
                                            );
                                            break;
                                    }
                                }
                            })
                        )
                    )
                )
                .Apply(eventNode.GetAttribute("fbcomments") == "true", __ => __
                    .AddAttribute("data-href", uri.Scheme + Uri.SchemeDelimiter + uri.Host + "/" + _lang + "/produktion/" + _submenu)
                    .AddAttribute("data-colorscheme", "light")
                    .Tag("div.fb-comments")
                )
                .Tag("nav.events-nav", () => _
                    .AddAttribute("href", "/" + _lang + "/produktion/" + prevEvent.GetAttribute("UID"))
                    .Tag("a.prev.ir", "prev")
                    .AddAttribute("href", "/" + _lang + "/produktion/" + nextEvent.GetAttribute("UID"))
                    .Tag("a.next.ir", "next")
                )
            )
            .AppendToPlaceHolder(pagePH);
        Assets.AddJavascript(this.Page, "site.addEvent('domready', function() { new lightbox(); new toggle(); })");
    }
}