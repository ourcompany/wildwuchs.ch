using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using ourCompany;
using System.Xml.XPath;

[PartialCaching(604800, "*", "", "infoAge")]
public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        IDataNode startpageNode = _data.SelectSingleNode("root/startpage");
        
        string[]
            imagesSrc = startpageNode.Select("image").Select(i => "'" + i.GetAttribute("src") + "?maxwidth=310" + "'").ToArray();

        new CSSControl()
        .Tag("div.startpage-text", _ => _
            .Tag("div.startpage-text-content", startpageNode.GetValue("text"))
        )
        .Tag("article.startpage", _ => _
            .Tag("div.row", () => _
                .Tag("figure.col.startimage-col")
                .Tag("figure.col.startimage-col")
            )
            .Tag("div.big-text-container", () => _
                .Tag("div.big-text-centering", () => _
                    .Tag("div.big-text", startpageNode.GetAttribute("bigtext"))
                )
            )
        )
        .AppendToPlaceHolder(pagePH);
        Assets.AddJavascript(Page, "site.addEvent('domready', function() {startPage([" + String.Join(",", imagesSrc) + "])});");
    }
}