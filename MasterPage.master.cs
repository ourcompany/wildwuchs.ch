using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ourCompany;
using System.Text;
using System.Xml.XPath;

[PartialCaching(604800, "*", "", "infoAge")]
public partial class MasterPage : MasterPageBase
{
    
    protected void Page_Init(object sender, EventArgs e)
    {
        string type = Request.QueryString["q"];

        IDataNode
            productions = _data.SelectSingleNode("root/programm/productions"),
            headerNode = _data.SelectSingleNode("root/header"),
            footerNode = _data.SelectSingleNode("root/footer");

        var
            pages = _data.Select("root/pages/page|root/pages/newsletter")
            .Where(m => (!m.GetAttribute("top").Check() || m.GetAttribute("top") != "true"));

        var
            firstMenus = _data.Select("root/pages/page|root/pages/newsletter")
            .Where(m => (m.GetAttribute("top").Check() && m.GetAttribute("top") == "true"));
        // 
        new CSSControl()
            .Tag("header.main-header", _ => _
                .Tag("div.header-content", () => _
                    .AddAttribute("href", "/")
                    .Tag("a", headerNode.GetAttribute("text"))
                )
            )
            .AppendToPlaceHolder(headerPH);

        new CSSControl()
            .Tag("nav.col.menu", _ => _
                .Apply(() =>
                {
                    if(firstMenus.Count() > 0) {
                        _.Tag("div.first-menus", () => _
                            .Apply(() => {
                                foreach (IDataNode page in firstMenus)
                                {
                                    switch (page.Name)
                                    {
                                        case "newsletter":
                                            _
                                            .AddAttribute("href", "/" + _lang + "/Newsletter")
                                            .Tag((_menu == "Newsletter" ? "h1" : "a") + ".menu-link.menu-page", "NEWSLETTER");
                                            break;
                                        default:
                                            _
                                            .AddAttribute("href", "/" + _lang + "/p/" + page.GetAttribute("UID"))
                                            .Tag((_menu == page.GetAttribute("UID") ? "h1" : "a") + ".menu-link.menu-page", page.GetAttribute("title"));
                                            break;
                                    }
                                }
                            })
                        );
                    }
                })
                /* static menus */
                .AddAttribute("href", "/" + _lang + "/kalender")
                .Tag("a.menu-link.menu-kalender" + (_menu == "Calendar" ? ".active" : ""), "kalender")
                .AddAttribute("href", "/" + _lang + "/produktionen")
                .Tag("a.menu-link.menu-produktionen" + (_menu == "Productions" ? ".active" : ""), "produktionen")
                /* dynamic pages */
                .Apply(() =>
                {
                    foreach (IDataNode page in pages)
                    {
                        switch (page.Name)
                        {
                            case "newsletter":
                                _
                                .AddAttribute("href", "/" + _lang + "/Newsletter")
                                .Tag((_menu == "Newsletter" ? "h1" : "a") + ".menu-link.menu-page", "NEWSLETTER");
                                break;
                            default:
                                _
                                .AddAttribute("href", "/" + _lang + "/p/" + page.GetAttribute("UID"))
                                .Tag((_menu == page.GetAttribute("UID") ? "h1" : "a") + ".menu-link.menu-page", page.GetAttribute("title"));
                                break;
                        }
                    }
                })
                .Tag("div.languages", () =>
                    _.Apply(() =>
                    {
                        if (_lang == "de")
                            _
                                .AddAttribute("href", cultureFinder.ChangeLang(Request, "de", "en"))
                                .Tag("a", "English");
                        else
                            _
                                .AddAttribute("href", cultureFinder.ChangeLang(Request, "en", "de"))
                                .Tag("a", "Deutsch");

                    })
                )
                .Tag("div.facebook-link", () => _
                    .AddAttribute("target", "_blank")
                    .AddAttribute("href", "http://www.facebook.com/wildwuchs")
                    .Tag("a", () => _
                        .AddAttribute("alt", "facebook")
                        .AddAttribute("src", "/assets/FACEBOOK.png")
                        .Tag("img")
                    )
                )
            )
            .AppendToPlaceHolder(menuPH);
        new CSSControl()
            .Tag("footer", _ => _
                .Tag("div.footer-content", footerNode.GetValue("text"))
            )
            .AppendToPlaceHolder(footerPH);
    }
}
