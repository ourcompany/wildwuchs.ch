﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using ourCompany;
using System.Xml.XPath;

[PartialCaching(604800)]
public class MasterPageBase : System.Web.UI.MasterPage
{
    public string _menu;
    public string _submenu;
    public string _lang;
    
    public int _cacheDuration = 10000;
    public List<string> _dataPath;

    public IDataNode _curNode;
    public string _title = "";

    public Data _data;
    public MasterPageBase()
    {
        this.Init += new EventHandler(InitPage);
    }
    
    protected void InitPage(object sender, EventArgs e)
    {
        Assets.Attach(this.Page);
        _lang = Request.Params["lang"] ?? "de";
        _menu = Request.Params["menu"] ?? "";
        _submenu = Request.Params["submenu"] ?? "";
        
        if (!Data.Languages.Instance.LanguageList.Any(s => s == _lang))
            _lang = cultureFinder.getLanguageString(Request, Data.Languages.Instance.LanguageList);
        _data = Data.Languages.Instance[_lang];
        _dataPath = new List<string>();
        _dataPath.Add(_data.Data.Path);
        _title = _data.SelectSingleNode("root/assetsHeader").GetAttribute("pageTitle");


    }

}