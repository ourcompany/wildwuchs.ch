﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ourCompany;
using System.Drawing;
using System.IO;

/// <summary>
/// Summary description for ImageSize
/// </summary>
public class ImageSize
{
    static readonly object Padlock = new object();
    private ImageSize()
    {
        
    }
    private Data _Sizes;
    private static ImageSize _Instance;
    public static ImageSize Instance
    {
        get
        {
            lock (Padlock)
            {
                if (_Instance == null) {
                    if (!File.Exists(new URL("/App_Data", "sizes.xml").ServerURL))
                        File.WriteAllText(new URL("/App_Data", "sizes.xml").ServerURL, "<store></store>");
                    _Instance = new ImageSize();
                    _Instance._Sizes = new Data("/App_Data/sizes.xml", "imageSize", "imageSize", false);
                }
                return _Instance;
            }
        }
    }
    public static Size size(URL image)
    {
        return size(image.ServerURL, "");
    }
    public static Size size(URL image, string queryString)
    {
        return size(image.ServerURL, queryString);
    }
    public static Size size(string ServerURL)
    {
        return size(ServerURL, "");
    }
    public static Size size(string ServerURL, string queryString)
    {
        IDataNode sizeNode = Instance._Sizes.SelectSingleNode("store/item[@path = '" + ServerURL + "?" + queryString + "']");
        if (sizeNode == null)
        {
            // try to get the image
            int fullHeight, fullWidth;
            try
            {

                using (Bitmap b = ImageResizer.ImageBuilder.Current.Build(ServerURL, new ImageResizer.ResizeSettings(queryString)))
                {
                    fullHeight = b.Height;
                    fullWidth = b.Width;
                }
            }
            catch (Exception)
            {
                return new Size(0, 0);
            }

            Instance._Sizes.MakeEdit();
            sizeNode = Instance._Sizes.SelectSingleNode("store").CreateChild("item");
            sizeNode.SetAttribute("path", ServerURL + "?" + queryString);
            sizeNode.SetAttribute("w", fullWidth.ToString());
            sizeNode.SetAttribute("h", fullHeight.ToString());
            Instance._Sizes.SaveChanges();
            Instance._Sizes.ReleaseEdit();
            
        }
        return new Size(int.Parse(sizeNode.GetAttribute("w")), int.Parse(sizeNode.GetAttribute("h")));

    }
}