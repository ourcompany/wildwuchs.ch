using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Mail;
using ourCompany;
using System.Web.UI;
using System.IO;
using System.Net;

/// <summary>
/// Handles mail sending
/// </summary>
public static class Mail
{
    public static bool sendEmail(CSSControl htmlEmail, StringBuilder textEmail, string mailto, string subject, string logoSrc, Attachment attcht)
    {
        IEnumerable<string> tos = (mailto).Split(',').Select(em => em.Trim());
        StringBuilder sb = new StringBuilder();
        HtmlTextWriter tw = new HtmlTextWriter(new StringWriter(sb));
        new CSSControl()
            .Render("html", false)
                .Render("head", false)
                    .AddAttribute("type", "text/css")
                    .Render("style", false)
                        .Write("body {background-color:#f4f4f4;color:#444444;font-size:13px; } ")
                        .Write(".fieldTitle {width:100px; color:#999999;} ")
                        .Write(".footer {padding:20px 0;} ")
                        .Write(".emailTitle {font-size:15px; font-weight:bold;padding-bottom:20px 0;} ")
                        .Write(".footer {padding:20px 0;} ")
                        .Write(".mainTable {background-image:url(http://" + HttpContext.Current.Request.Url.Host + logoSrc + ");width:600px; background-position:right 10px; background-repeat:no-repeat; background-color:white; margin:25px 50px 150px 25px; padding:15px;box-shadow: -1px 1px 3px rgba(0,0,0,0.3);} ")
                    .closeTag()
                .closeTag()
                .Render("body", false)
                    .Render("table.mainTable", false)
                        .Render("tr", false)
                            .Render("td.emailTitle", subject)
                        .closeTag()
                        .Render("tr", false)
                            .Render("td", false)
                                .Render("table", false)
                                    .AppendCSSControl(htmlEmail)
                                .closeTag()
                            .closeTag()
                        .closeTag()
                    .closeTag()
                .closeTag()
            .closeTag()
            .RenderToWriter(tw);

        MailMessage toSend = new MailMessage()
        {
            From = new MailAddress(constant.EMAIL_FROM),
            Subject = subject,
            Body = textEmail.ToString()
        };
        if (attcht != null)
            toSend.Attachments.Add(attcht);
        toSend.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(sb.ToString(), new System.Net.Mime.ContentType("text/html")));
        foreach (string to in tos)
        {
            toSend.To.Add(to);
        }

        SmtpClient mailSending = new SmtpClient(constant.EMAIL_SMTP_HOST, constant.EMAIL_SMTP_PORT);

        mailSending.EnableSsl = constant.EMAIL_SMTP_SSL;
        mailSending.Timeout = 10000;
        mailSending.DeliveryMethod = SmtpDeliveryMethod.Network;
        mailSending.UseDefaultCredentials = false;
        mailSending.Credentials = new NetworkCredential(constant.EMAIL_SMTP_USER, constant.EMAIL_SMTP_PASSWORD);

        try
        {
            mailSending.Send(toSend);
            return true;
        }
        catch
        {
            return false;
        }
    }
}