﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
/// <summary>
/// Summary description for cultureFinder
/// </summary>
public class cultureFinder
{
    public static string getLanguageString(HttpRequest req, List<string> availableLangs)
    {
        List<string> languages = req.UserLanguages != null ? new List<string>(req.UserLanguages) : null;
        if (languages == null || languages.Count == 0)
            return availableLangs.First();
        foreach (string l in languages)
        {
            string returnLang = availableLangs.FirstOrDefault(lang => lang.ToLowerInvariant() == l.ToLowerInvariant());
            if (returnLang != null)
                return returnLang;
        }
        return availableLangs.First();
    }
    public static string ChangeLang(HttpRequest req, string curLang, string newLang)
    {
        Regex R = new Regex("^(?:/" + curLang + ")(?<rest>/.+)?$");
        Match M = R.Match(req.RawUrl);
        return "/" + newLang + (M.Success ? M.Groups["rest"].Value : "");

    }
}