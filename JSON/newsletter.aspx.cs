﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ourCompany;
using createsend_dotnet;
using Newtonsoft.Json;

public partial class ajax_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string
            name = Request.Params["name"],
            email = Request.Params["email"];

        JsonWriter
            writer = new JsonTextWriter(Response.Output);

        if (!(email.Check() && name.Check()))
        {
            writer.WriteStartObject();
            writer.WritePropertyName("success");
            writer.WriteValue(false);
            writer.WritePropertyName("message");
            writer.WriteValue("email or name empty");
            writer.WriteEndObject();
            writer.Flush();
            writer.Close();
            return;
        }

        try
        {
            Subscriber newSub = new Subscriber(constant.CAMPAIGN_MONITOR_LIST_ID);
            newSub.Add(email, name, null, true);
        }
        catch (CreatesendException ex)
        {
            ErrorResult error = (ErrorResult)ex.Data["ErrorResult"];
            writer.WriteStartObject();
            writer.WritePropertyName("error");
            writer.WriteValue(error.Code + " - " + error.Message);
            writer.WriteEndObject();
            writer.Flush();
            writer.Close();
            return;
        }
        catch (Exception ex)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("error");
            writer.WriteValue(ex.ToString());
            writer.WriteEndObject();
            writer.Flush();
            writer.Close();
            return;
        }

        writer.WriteStartObject();
        writer.WritePropertyName("success");
        writer.WriteValue(true);
        writer.WriteEndObject();
        writer.Flush();
        writer.Close();
    }
}