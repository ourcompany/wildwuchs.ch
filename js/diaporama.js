﻿var diaporama = new Class({
    Implements: [Options, Events],
    _slidesNumber: 0,
    _curSlideNum: -1,
    _prevSlideNum: -2,
    _curZ: 1,
    _inte: false,
    options: {
        container: false,
        transition: {
            cssShow: "in",
            cssHide: "out",
            duration: 500
        },
        modifyContainerStyles: { }, // {"y": "height", "x": "width" }
        delay: 6000,
        eltpath: false,
        linkpath: false,
        autostart: false,
        UID: false,
        initSlideNum: 0,
        events: false,
        keyboard: false
    },
    /* events :
    change : function(curInd, numSlides) {},
    beforeShow : function(eltShown, curInd) {},
    afterHide : function(eltHidden) {}
    */
    _call: function (o) {
        this._stop();
        if (this.options.UID && this.options.UID != o.UID)
            return;

        if (o.a || o.a == 0)
            o = o.a;
        switch (typeOf(o)) {
            case "string":
                switch (o) {
                    case "next":
                        this._moveNext();
                        break;
                    case "back":
                        this._moveBack();
                        break;
                    case "play":
                        this._play();
                        break;
                }
                break;
            case "number":
                this._moveTo(o);
                break;
        };
    },
    initialize: function (options) {
        this.init(options);

        site.addEvent('diaporama', function (i) { this._call(i); } .bind(this));
    },
    init: function (options) {
        this.setOptions(options);
        if (this.options.events)
            this.addEvents(this.options.events);
        if (this._inte)
            $clear(this._inte);

        if (this.options.container && $(this.options.container).retrieve("fx"))
            $(this.options.container).eliminate('fx');

        this._curZ = 1;

        if (!$(this.options.container) || !(this.options.eltpath))
            return;

        var imagesArr = $(this.options.container).getElements(this.options.eltpath);
        this._slidesNumber = imagesArr.length;
        if (this._slidesNumber == 0)
            return;
        if (Browser.isMobile) {

            //$(this.options.container).addEvent("swipe", function (e) { switch (e.direction) { case "left": this._moveBack(); break; case "right": this._moveNext(); break; } } .bind(this))
            $(this.options.container).addEvent("click", this._moveNext.bind(this));
        }

        this._curSlideNum = Math.min(this.options.initSlideNum, this._slidesNumber - 1);

        imagesArr[this._curSlideNum].setStyle('z-index', this._curZ);
        this._curZ++;
        this._initAnimations();

        this._animate(this._curSlideNum);
        this.fireEvent("change", [this._curSlideNum, this._slidesNumber]);
        if (this.options.linkpath)
            this._hightlightLink();
        if (this.options.autostart)
            this._inte = this._moveNext.periodical(this.options.delay, this);
    },
    _initAnimations: Modernizr.cssanimations ? function () {
        //      nothing to initialize.
    } : function () {

        var eltArray = $(this.options.container).getElements(this.options.eltpath);
        eltArray.push($(this.options.container));
        var fx = new Fx.Elements(eltArray, { duration: this.options.transition.duration });

        if (this.options.onAfterHide) {
            fx.addEvent("complete", function () {
                if (this._prevSlideNum >= 0)
                    this.fireEvent("afterHide", eltArray[this._prevSlideNum]);
            } .bind(this));
        }

        $(this.options.container).store("fx", fx);

    },
    _animate: Modernizr.cssanimations ? function (slideNum) {

        $(this.options.container).getElements(this.options.eltpath).each(function (slide, ind) {
            if (ind == this._curSlideNum && this._curSlideNum != slideNum) {
                slide.removeClass(this.options.transition.cssShow);
                this._prevSlideNum = ind;
                this.fireEvent("afterHide", slide);
                slide.addClass(this.options.transition.cssHide);
            } else if (ind == slideNum) {
                slide.setStyle("z-index", this._curZ++);
                var dim = slide.getDimensions();
                Object.each(this.options.modifyContainerStyles, function (val, key) {
                    $(this.options.container).setStyle(val, dim[key]);
                }, this);
                this.fireEvent("beforeShow", [slide, slideNum]);
                slide.removeClass(this.options.transition.cssHide);
                slide.addClass(this.options.transition.cssShow);
            }
            else {
                slide.removeClass(this.options.transition.cssShow);
            }
        }, this);

    } : function (slideNum) {

        var fxObjSet = {}, fxObjStart = {}, fx = $(this.options.container).retrieve("fx");

        $(this.options.container).getElements(this.options.eltpath).each(function (slide, ind) {

            if (ind == this._curSlideNum && this._curSlideNum != slideNum) { fxObjStart[ind] = { "opacity": 0 }; this._prevSlideNum = ind; }
            else if (ind == slideNum) {
                slide.setStyle("z-index", this._curZ++);
                var dim = slide.getDimensions();
                Object.each(this.options.modifyContainerStyles, function (val, key) {
                    if (!fxObjStart[this._slidesNumber])
                        fxObjStart[this._slidesNumber] = {};
                    fxObjStart[this._slidesNumber][val] = dim[key];
                }, this);
                fxObjStart[ind] = { "opacity": 1 };
                this.fireEvent("beforeShow", [slide, slideNum]);
            }
            else { fxObjSet[ind] = { "opacity": 0 }; }
        }, this);

        fx.cancel();
        if (this.options.transition.duration > 0) {

            fx
                .set(fxObjSet)
                .start(fxObjStart);
        }
        else {
            fx.set(fxObjStart);
        }

    },
    _stop: function () {
        if (this._inte)
            $clear(this._inte);
    },
    _play: function () {
        if (!this._inte) {
            this._inte = this._moveNext.periodical(this.options.delay, this);
        }
    },
    _moveNext: function () {
        this._moveTo((this._curSlideNum + 1) % this._slidesNumber);
    },
    next: function (loop) {
        if (loop || (this._curSlideNum + 1) < this._slidesNumber) {
            this._moveNext();
            return true;
        } else
            return false;
    },
    _moveBack: function () {
        this._moveTo((this._curSlideNum - 1 + this._slidesNumber) % this._slidesNumber);
    },
    back: function (loop) {
        if (loop || (this._curSlideNum - 1) >= 0) {
            this._moveBack();
            return true;
        } else
            return false;
    },
    moveTo: function (slideNum) {
        return this._moveTo(slideNum);
    },
    _moveTo: function (slideNum) {
        if (slideNum == this._curSlideNum)
            return false;
        this._animate(slideNum);
        this._curSlideNum = slideNum;
        this.fireEvent("change", [this._curSlideNum, this._slidesNumber]);
        if (this.options.linkpath)
            this._hightlightLink();
        return true;
    },
    _hightlightLink: function () {
        $(this.options.container).getElements(this.options.linkpath).each(function (aElt, ind) {
            if (ind == this._curSlideNum) {
                aElt.addClass('active');
            }
            else {
                aElt.removeClass('active');
            }
        }, this);
    },
    destroy: function () {
        if (this._inte)
            $clear(this._inte);
        var fx;
        if (this.options.container && (fx = $(this.options.container).retrieve("fx"))) {
            fx.cancel();
            $(this.options.container).eliminate('fx');
        }
    }
});
