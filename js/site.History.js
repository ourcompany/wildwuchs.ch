﻿/*
    add to the history manager without firing history event.
*/
History.add = History.hasPushState() ?
    function (url, title, state) {
        url = History.cleanURL(url);
        if (History.cleanURL(window.location.href) == url)
            return;
        window.history.pushState(state || null, title || null, url);
    } : function (url) {
        var url = cleanURL(url);
        if (History.cleanURL(window.location.href) == url)
            return;
        History.hash = url;
        location.hash = url;
    };
/*
    Example of implementation :
    
    // upon initialization :
        site.addEvent("history", this._historyChange.bind(this));

    // handle no state object => retrive info via URL
    _historyRegex: /p\/([^\/]+)(?=(\/|$))/gi,
    // handle history change 
    _historyChange: function (url, state, hash) {
        if (!state || !state.UID) {
            var urlInfos = this._historyRegex.exec(url);
            if (urlInfos && urlInfos.length >= 2)
                state = { "UID": urlInfos[1] };
            else
                return;
        }
    }, 
    // add an history step
    _changesMade : function(uid) {
        site.setHistory(url, title, {UID : uid}});
    }
*/
Object.append(site,
    {
        _historyObject: {},
        _HISTORY_TITLE: "site._HISTORY_TITLE",
        _addHistory: function (o) {
            History.addEvent('change', function (url, state) {
                
                if (!History.hasPushState()) {
                    state = this._historyObject[url];
                }
                if (state && state[this._HISTORY_TITLE])
                    document.title = state[this._HISTORY_TITLE];
                this.fireEvent('history', [url, state]);
            }.bind(this));
            if (!History.hasPushState()) {
                // Check if there is a hash
                var hash = document.location.hash.substr(1);
                if (!hash) return;

                // If the hash equals the current page, don't do anything
                var path = document.location.pathname;
                var url = o ? (o.url ? o.url : "") : "";
                if (url + hash == path) return;
                this.fireEvent('history', [url, null, hash]);
            }

        },
        get: function (url, title, state, hashValue) {
            state = (state || {});
            state[this._HISTORY_TITLE] = title;
            if (!History.hasPushState()) {
                this._historyObject[hashValue] = state;
                location.hash = hashValue;
            } else {
                History.push(url, title, state);
            }
        },
        setHistory: function (url, title, state) {
            state = (state || {});
            state[this._HISTORY_TITLE] = title;
            History.add(url, title, state);
        }
    });
History.handleInitialState();

site.addEvent("domready", site._addHistory.bind(site));