﻿var shortcuts = new Class({
    initialize: function (o) {
        ["up", "left", "right", "down"].each(function (dir) {
            if (o && o[dir])
                site.addKeyboardEvent(dir, this._go.pass(o[dir], this));
        }, this);

        $$("*[data-swipe-left]").each(function (a) {
            var f = function (e) {
                e.preventDefault();
                if (e.direction == "left")
                    this._go(a.get("data-swipe-left"));
            } .bind(this);
            a
                .addEvent("swipe", f)
                .addEvent('touchmove', function (event) {
                    event.preventDefault();
                });
        }, this);
        $$("*[data-swipe-right]").each(function (a) {
            var f = function (e) {
                e.preventDefault();
                if (e.direction == "right")
                    this._go(a.get("data-swipe-right"));
            } .bind(this);
            a.addEvent("swipe", f);
        }, this);
    },
    _go: function (url) {
        window.location = url;
    }
});