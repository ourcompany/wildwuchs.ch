var site = {
    lang: false,
    domready: function (o) {
        this.lang = o ? (o.lang ? o.lang : "") : "";
        this.fireEvent('beforedomready', o);
        this.fireEvent('domready');
    },
    call: function (action, args) {
        this.fireEvent(action, args);
        return void (0);
    }
};
Object.append(site, new Events());

window.addEvent("domready", function () { site.domready(); });  