﻿Object.append(site,
    {
        _windowSize: false,
        _resizeAdded: false,
        _addResize: function () {
            if (site._resizeAdded)
                return;
            window.addEvent("resize", function () {
                if (site._resizeInterval)
                    clearTimeout(site._resizeInterval);
                site.resizeInterval = site._resize.delay(site._resizeDelay);
            });
            site._resizeAdded = true;
            site._windowSize = window.getSize();
        },
        _resize: function () {
            site._windowSize = window.getSize();
            site.fireEvent("resize", site._windowSize);
        },
        _resizeInterval: false,
        _resizeDelay: 200
    });
site.addEvent("beforedomready", site._addResize.bind(site));