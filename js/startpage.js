﻿function startPage(images) {
    $$("figure.startimage-col").each(function (toFill) {
        var filled = false;
        while (images.length > 0 && !filled) {
            var randomSrc = images.getRandom();
            if (randomSrc) {
                toFill.adopt(new Element("img", { "src": randomSrc }));
                images.erase(randomSrc);
                break;
            }
        }
    });
}