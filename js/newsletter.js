﻿var newsletter = new Class({
    _req: false,
    initialize: function () {
        site.addEvent('newsletter', function (o) { this._call(o); }.bind(this))
        this._req = new Request.JSON({ 'url': '/JSON/newsletter.aspx' }).addEvent('complete', this._update.bind(this));
    },
    _call: function (o) {
        switch (o.a) {
            case "send":
                this._checkFields();
                break;
        }
    },
    _checkFields: function () {
        var pass = true;
        $('newsletter-form').getElements("input.to-check").each(function (inputElt) {
            var value = inputElt.get('value');
            if (!value) {
                pass = false;
                inputElt.addClass('fail');
            }
            else {
                inputElt.removeClass('fail');
                if (inputElt.get('name') == 'email') {
                    if (!(value.contains('@') && value.contains('.'))) {
                        pass = false;
                        inputElt.addClass('fail');
                    }
                }
            }
        });
        if (pass)
            this._send();
    },
    _send: function () {
        var
            container = $('newsletter-form'),
            name = container.getElement('input[name=name]').get('value'),
            email = container.getElement('input[name=email]').get('value');

        this._req.post({ "name": name, "email": email });
        $('send').setStyle("display", "none");
        $('loader').addClass('show');
    },
    _update: function (o) {
        $('loader').removeClass('show');
        if (o.success) {
            $('thanks').addClass('show');
            $('success').addClass('show');
        }
        else {
            $('send').setStyle("display", "inline-block");
        }
    }
});