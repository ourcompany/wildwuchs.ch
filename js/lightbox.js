﻿var lightbox = new Class({
    _container: false,
    _figure: false,
    initialize: function () {
        this._container = $('lightbox-container'),
        this._figure = this._container.getElement('figure.lightbox-content');
        
        site.addEvent('lightbox', function (o) { this._call(o); }.bind(this));
        this._container.addEvent("click", function () { this._close(); }.bind(this));
        this._figure.addEvent("click", function (e) { e.stopPropagation(); });
    },
    _call: function (o) {
        switch (o.a) {
            case "open":
                this._open(o.i);
                break;
            case "close":
                this._close();
                break;
            case "video":
                this._video(o.i);
                break;
            case "diaporama":
                this._diaporama(o.i);
                break;
        }
    },
    _open: function (src) {
        this._figure.adopt(new Element("img", { "src": src + "?maxwidth=950&maxheight=470" }));
        this._figure.adopt(new Element("a", { "html": "close", "href" : "javascript:site.call('lightbox', { a : 'close'})", "class" : "ir close" }));
        this._container.addClass('show');
    },
    _video: function (src) {
        var
            video = src.split(','),
            iframe = false;
        switch (video[0]) {
            case "youtube":
                iframe = new Element("iframe", { "width": "950", "height": "470", "src": "http://www.youtube.com/embed/" + video[1], "frameborder" : "0", "allowfullscreen" : "1", "autoplay" : "1", "rel" : "0" })
                break;
            case "vimeo":
                iframe = new Element("iframe", { "width": "950", "height": "470", "src": "http://player.vimeo.com/video/" + video[1], "frameborder": "0", "webkitAllowFullScreen" : "1", "allowFullScreen" : "1", "mozallowfullscreen" : "1" })
                break;
        }
        this._figure.adopt(new Element("a", { "html": "close", "href": "javascript:site.call('lightbox', { a : 'close'})", "class": "ir close" }));
        this._figure.adopt(iframe);
        this._container.addClass('show');
    },
    _diaporama: function (srcArray) {
        var diaporamaContainer = new Element("div.diaporama-outer");
        for (var i = 0; i < srcArray.length; i++) {
            var slide = new Element("figure.slide").adopt(new Element("img", { "src": srcArray[i] + "?maxwidth=950&maxheight=470" }));
            diaporamaContainer.adopt(slide);
        }

        if(srcArray.length > 1) {
            var diap = new diaporama({
                container: diaporamaContainer,
                eltpath: "figure.slide"
            });

            var nextElt = new Element("a.diaporama-nav.next", { "href": "javascript:void(0)" });
            var prevElt = new Element("a.diaporama-nav.previous", { "href": "javascript:void(0)" });

            nextElt.addEvent("click", diap.next.bind(diap));
            prevElt.addEvent("click", diap.back.bind(diap));

            this._figure
                .adopt(prevElt)
                .adopt(nextElt);
        }

        this._figure.adopt(new Element("a", { "html": "close", "href": "javascript:site.call('lightbox', { a : 'close'})", "class": "ir close" }));
        this._figure.adopt(diaporamaContainer);
        this._container.addClass("show");
    },
    _close: function () {
        this._container.removeClass('show');
        this._figure.empty();
    }
});