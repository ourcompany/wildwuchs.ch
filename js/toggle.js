﻿var toggle = new Class({
    initialize: function () {
        site.addEvent('toggle', function (o) { this._toggle(o.l, o.c) }.bind(this));
    },
    _toggle: function (linkClass, contentClass) {
        $('container').getElements("a." + linkClass).each(function (aElt) {
            if (aElt.hasClass('active')) {
                aElt.removeClass('active');
                this._hideContent(contentClass);
            }
            else {
                aElt.addClass('active');
                this._showContent(contentClass);
            }
        }, this);
    },
    _showContent: function (contentClass) {
        $('container').getElements("div." + contentClass).each(function (Elt) {
            Elt.addClass('show');
        });
    },
    _hideContent: function (contentClass) {
        $('container').getElements("div." + contentClass).each(function (Elt) {
            Elt.removeClass('show');
        });
    },
});