﻿Object.append(site,
    {
        _scrollAdded: false,
        _addScroll: function () {
            if (!site._resizeAdded) {
                console.log("site.Scroll needs site.Size to work...");
                return;
            }

            if (site._scrollAdded)
                return;

            site._addResize();
            window.addEvent("scroll", this._onScroll.bind(this));
            site._scrollAdded = true;
        },
        _onScroll: function () {
            if (!site._listenToScroll)
                return;

            if ((window.getScrollSize().y - site._windowSize.y - window.getScroll().y) < site._scrollOffset) {
                site.fireEvent("scroll", "bottom");
            } else if (window.getScrollSize().y < site._scrollOffset) {
                site.fireEvent("scroll", "top");
            }

            if (window.getScrollSize().x < site._scrollOffset) {
                site.fireEvent("scroll", "left");
            } else if ((window.getScrollSize().x - site._windowSize.x - window.getScroll().x) < site._scrollOffset) {
                site.fireEvent("scroll", "right");
            }
        },

        _listenToScroll: true,
        _scrollOffset: 200,
        attachScrollListener: function () {
            site._listenToScroll = true;
        },
        detachScrollListener: function () {
            site._listenToScroll = false;
        }
    });
site.addEvent("beforedomready", site._addScroll.bind(site));