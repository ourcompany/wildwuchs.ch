﻿var imageLoader = new Class({
    _sizes: [
        { x: 1000, y: 750 },
    ],
    _min: {
        x: 0,
        y: 0
    },
    initialize: function () {
        site.addEvent("resize", this._adapt.bind(this));
        $$("img[data-img-loader-load]").each(function (img) {
            img
                .addClass("img-loader-loading")
                .removeClass("img-loader-init");
        });
        site.addEvent('load', function () { this._adapt(); } .bind(this));
    },
    _adapt: function () {
        $$("img[data-img-loader-load]").each(function (img) {
            var src = img.get("data-img-loader-load"),
                size = Object.map(this._getDesiredSize(img), function (value, key) { return Math.max(this._min[key], value); } .bind(this)),
                i = 0;

            while (this._sizes[i].x < size.x && this._sizes[i].y < size.y && i < this._sizes.length) {
                i++;
            }

            var toLoad = src + "?maxwidth=" + this._sizes[i].x + "&maxheight=" + this._sizes[i].y;
            if (img.get("src") == toLoad || img.get("data-slider-src") == toLoad) {

                var
                   dataMaxWidth = img.get("data-img-loader-maxwidth"),
                   dataMaxHeight = img.get("data-img-loader-maxheight"),
                   maxWidth = dataMaxWidth ? Number(dataMaxWidth) : this._sizes[i].x,
                   maxHeight = dataMaxHeight ? Number(dataMaxHeight) : this._sizes[i].y,
                   iscaleWidth = size.x / maxWidth < 1,
                   iscaleHeight = size.y / maxHeight < 1,
                   scale = Math.min(Math.min(size.x / maxWidth, size.y / maxHeight), 1);

                img
                   .setStyles({
                       "width": iscaleWidth ? scale * maxWidth : "auto",
                       "height": iscaleHeight ? scale * maxHeight : "auto"
                   });
                return;
            }

            img
                .set("data-img-loader-src", toLoad)
                .removeClass("img-loader-loaded")
                .addClass("img-loader-loading");

            Asset.image(toLoad, {
                onLoad: function (e) { this._preloadComplete(e, size, img); } .bind(this)
            });
        }, this);
    },
    _getDesiredSize: function (img) {
        return $("left-col").getSize();
    },
    _preloadComplete: function (e, size, img) {

        if (img.get("data-img-loader-src") != e.get("src"))
            return;
        // fit in : 
        var 
            maxWidth = Number(e.get("width")),
            maxHeight = Number(e.get("height")),
            iscaleWidth = size.x / maxWidth < 1,
            iscaleHeight = size.y / maxHeight < 1,
            scale = Math.min(Math.min(size.x / maxWidth, size.y / maxHeight), 1);

        img
            .set({
                "src": e.get("src"),
                "data-img-loader-maxwidth": maxWidth,
                "data-img-loader-maxheight": maxHeight
            })
            .setStyles({
                "width": iscaleWidth ? scale * maxWidth : "auto",
                "height": iscaleHeight ? scale * maxHeight : "auto"
            })
            .removeClass("img-loader-loading")
            .addClass("img-loader-loaded");
    }
});