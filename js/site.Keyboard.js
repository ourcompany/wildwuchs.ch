﻿Object.append(site,
    {
        _keyBoardAdded: false,
        _keyboard: false,
        _addKeyboard: function () {
            if (site._keyBoardAdded)
                return;
            site._keyboard = new Keyboard({
                defaultEventType: 'keydown'
            })
            site._keyboard.activate();
            site._keyBoardAdded = true;
        },
        addKeyboardEvent: function (name, handler) {
            site._keyboard.addEvent(name, handler);
            return site;
        },
        removeKeyboardEvents: function (name) {
            site._keyboard.removeEvents(name);
            return site;
        },
        attachKeyboard: function () {
            site._keyboard.activate();
        },
        detachKeyboard: function () {
            site._keyboard.deactivate();
        }
    });
site.addEvent("beforedomready", site._addKeyboard.bind(site));